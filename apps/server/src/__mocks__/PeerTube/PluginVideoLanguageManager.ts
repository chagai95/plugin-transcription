import { PluginVideoLanguageManager } from '@peertube/peertube-types';

export const pluginVideoLanguageManager: PluginVideoLanguageManager = {
  getConstants: jest.fn(() => ({
    fr: 'French',
    en: 'English',
  })),
  addConstant: jest.fn(),
  deleteConstant: jest.fn(),
  resetConstants: jest.fn(),
  getConstantValue: jest.fn(),
  addLanguage: jest.fn(),
  deleteLanguage: jest.fn(),
};
