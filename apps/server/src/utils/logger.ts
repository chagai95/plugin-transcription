import { createLogger, format, transports } from 'winston';
import { parse } from 'bytes';

function getLoggerReplacer() {
  const seen = new WeakSet();

  // Thanks: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Errors/Cyclic_object_value#Examples
  return (key: string, value: any) => {
    if (key === 'cert') return 'Replaced by the logger to avoid large log message';

    if (typeof value === 'object' && value !== null) {
      if (seen.has(value)) return;

      seen.add(value);
    }

    if (value instanceof Set) {
      return Array.from(value);
    }

    if (value instanceof Map) {
      return Array.from(value.entries());
    }

    if (value instanceof Error) {
      const error = {};

      Object.getOwnPropertyNames(value).forEach((key) => {
        // @ts-ignore
        error[key] = value[key];
      });

      return error;
    }

    return value;
  };
}

const consoleLoggerFormat = format.printf((info) => {
  const { label, timestamp, level, message, sql, tags, ...obj } = info;

  let additionalInfos = JSON.stringify(obj, getLoggerReplacer(), 2);

  if (additionalInfos === undefined || additionalInfos === '{}') additionalInfos = '';
  else additionalInfos = ' ' + additionalInfos;

  return `[${info.label}] ${info.timestamp} ${info.level}: ${info.message}${additionalInfos}`;
});

const jsonLoggerFormat = format.printf((info) => {
  return JSON.stringify(info, getLoggerReplacer());
});

const timestampFormatter = format.timestamp({
  format: 'YYYY-MM-DD HH:mm:ss.SSS',
});

export interface WorkerLoggerOptions {
  level: string;
  filename: string;
  threadId: number;
}

export function createWorkerLogger({ level, filename, threadId }: WorkerLoggerOptions) {
  const fileTransport = new transports.File({
    filename,
    handleExceptions: true,
    format: format.combine(format.timestamp(), jsonLoggerFormat),
    maxsize: parse('12MB'),
    maxFiles: 20,
  });

  const logger = createLogger({
    level,
    format: format.combine(format.label({ label: `Worker n°${threadId}` }), format.splat()),
    transports: [
      fileTransport,
      new transports.Console({
        handleExceptions: true,
        format: format.combine(timestampFormatter, format.colorize(), consoleLoggerFormat),
      }),
    ],
    exitOnError: true,
  });

  // Winston quirk, is it really useful?
  logger.on('error', () => {
    if (!logger.transports.includes(fileTransport)) {
      logger.add(fileTransport);
    }
  });

  return logger;
}
