import { extractLanguage, LOCALE_REGEXP } from './i18n';

describe('i18n', () => {
  test('localeRegexp', async () => {
    expect(LOCALE_REGEXP.test('en-us')).toBeTruthy();
    expect(LOCALE_REGEXP.test('en-uk')).toBeTruthy();
  });

  test('extractLanguage', async () => {
    expect(extractLanguage('en-us')).toEqual('en');
    expect(extractLanguage('en-uk')).toEqual('en');
  });
});
