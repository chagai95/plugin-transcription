import https from 'https';
import http, { ClientRequest, IncomingMessage, OutgoingHttpHeaders, RequestOptions } from 'http';
import { URL } from 'url';
import { Readable } from 'stream';
import FormData from 'form-data';

export function isSSL(protocol?: string | null) {
  return protocol === 'https:';
}

export function guessAdapterFromURL({ protocol }: Pick<URL, 'protocol'>) {
  return isSSL(protocol) ? https : http;
}

export function get(url: string, headers?: OutgoingHttpHeaders | undefined) {
  const urlObject = new URL(url);
  const { hostname, port, pathname, search, protocol } = urlObject;
  const adapter = guessAdapterFromURL(urlObject);
  return new Promise((resolve, reject) => {
    adapter
      .get(
        {
          host: hostname,
          port,
          path: `${pathname}${search}`,
          protocol,
          method: 'GET',
          headers,
        },
        (res) => {
          let responseBody = '';
          // res.setEncoding('utf8');
          res.on('data', (chunk) => (responseBody += chunk.toString()));
          res.on('end', () => resolve(responseBody));
        }
      )
      .on('error', reject)
      .end();
  });
}

export function getStreamPromise(url: string): Promise<Readable> {
  const urlObject = new URL(url);
  const adapter = guessAdapterFromURL(urlObject);

  return createResponsePromise(adapter.get(urlObject)) as Promise<Readable>;
}

export function getRequestedURL(request: ClientRequest) {
  return `${request.protocol}//${request.host}${request.path}`;
}

export function createResponsePromise(request: ClientRequest) {
  return new Promise((resolve, reject) => {
    request
      .on('response', async (response) => {
        const { statusCode, statusMessage } = response;
        try {
          if (!statusCode) {
            throw new Error(`No status code is present on the request.`);
          }

          if (statusCode === 301 || statusCode === 302) {
            throw new Error(`Couldn't handle redirection to ${response.headers.location}.`);
          }

          if (statusCode >= 400) {
            const responseBody = await createResponseEndPromise(response);
            throw new Error(
              `${request.method} ${getRequestedURL(request)} ${statusCode} ${statusMessage} ${responseBody}.`
            );
          }

          resolve(response);
        } catch (e) {
          reject(e);
        }
      })
      .on('error', (e) => {
        reject(e);
      });
  });
}

export function createResponseEndPromise(response: IncomingMessage) {
  return new Promise((resolve, reject) => {
    let responseBody = '';
    // res.setEncoding('utf8');
    response.on('data', (chunk) => (responseBody += chunk.toString()));
    response.on('end', () => resolve(responseBody));
    response.on('error', reject);
  });
}

export function createRequest(options: RequestOptions, formData?: FormData) {
  if (formData) {
    options.headers = {
      ...options.headers,
      ...formData.getHeaders(),
    };
  }

  const adapter = isSSL(options.protocol) ? https : http;
  const request = adapter.request(options);

  if (formData) {
    formData.pipe(request);
  }
  return request;
}

export function request(options: RequestOptions, formData?: FormData) {
  const request = createRequest(options, formData);

  return createResponsePromise(request);
}
