import fs from 'fs';
import { Model } from 'vosk';
import { resolve } from 'path';
import { cwd } from 'process';

export class ModelFactory {
  private modelPath: string;

  constructor(modelPath: string) {
    this.modelPath = modelPath;
    const fullPath = resolve(cwd(), this.modelPath);
    if (!fs.existsSync(fullPath)) {
      console.log(
        'Please download the model from https://alphacephei.com/vosk/models and unpack as ' +
          fullPath +
          ' in the current folder.'
      );
      // process.exit();
    }
  }

  createFromLanguage(language: string) {
    const fullPath = resolve(cwd(), this.modelPath, language);
    if (!fs.existsSync(fullPath)) {
      console.log(
        'Please download the model from https://alphacephei.com/vosk/models and unpack as ' +
          fullPath +
          ' in the current folder.'
      );
      // process.exit();
    }

    return new Model(fullPath);
  }
}
