import { VoskModelPageParser } from './VoskModelPageParser';

describe('VoskModelParser', () => {
  test('parse Vosk models page', async () => {
    const voskModelPageParser = new VoskModelPageParser(
      'https://alphacephei.com/vosk/models/model-list.json',
      console,
      ['fr', 'en']
    );
    const models = await voskModelPageParser.parse();
    expect(Array.isArray(models)).toBe(true);
    expect(models.length).toBeGreaterThan(0);
  });
});
