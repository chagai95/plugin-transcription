import { mkdirp } from 'fs-extra';
import { resolve } from 'path';
import { finished } from 'stream/promises';
import { MessagePort } from 'worker_threads';
import { TranscriptionRequestInterface } from './TranscriptionRequestInterface';
import { SubtitleGenerator } from '../Subtitle/SubtitleGenerator';
import { LoggerInterface } from '../Model';

export interface TranscriptionResult {
  uuid: string;
  language: string;
  filePath: string;
}

export class TranscriptionRequestProcessor {
  private parentPort: MessagePort;
  private subtitleGenerator: SubtitleGenerator;
  private captionsPath: string;
  private logger: LoggerInterface;

  constructor(
    parentPort: MessagePort,
    subtitleGenerator: SubtitleGenerator,
    captionsPath: string,
    logger: LoggerInterface
  ) {
    this.parentPort = parentPort;
    this.subtitleGenerator = subtitleGenerator;
    this.captionsPath = captionsPath;
    this.logger = logger;
  }

  public async process({ uuid, language, filePath }: TranscriptionRequestInterface) {
    this.logger.info(`Processing transcription request for video "${uuid}" in "${language}".`);
    await mkdirp(this.captionsPath);
    const outputFilePath = resolve(this.captionsPath, `${uuid}-${language}.vtt`);
    this.logger.info(`From ${filePath} to ${outputFilePath}`);
    const captionFileStream = this.subtitleGenerator.generateToFile({
      inputFilePath: filePath,
      language,
      outputFilePath,
    });
    if (!captionFileStream) {
      throw new Error('Undefined caption file stream');
    }

    await finished(captionFileStream);

    this.parentPort.postMessage(outputFilePath);
  }
}
