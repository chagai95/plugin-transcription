import FormData from 'form-data';
import { URL } from 'url';
import { MOAuthTokenUser } from '@peertube/peertube-types/server/types/models';
import { ResultList, VideoCaption } from '@peertube/peertube-types';
import { PeerTubeApiInterface } from './PeerTubeApiInterface';
import { get, request } from '../utils';
import { LoggerInterface, PeerTubeAboutConfigInterface } from '../Model';

export interface AuthenticatedPeerTubeResponse extends Express.Response {
  locals: { oauth: { token: MOAuthTokenUser } };
}

export interface ErrorResponseInterface {
  type: string;
  detail?: string;
  status: number;
  code?: string;
  error: string;
}

export function isAnErrorResponse(obj: any): obj is ErrorResponseInterface {
  return obj.error !== undefined;
}

export class PeerTubeApi implements PeerTubeApiInterface {
  private logger: LoggerInterface;
  private webserverUrl: URL;
  private oAuthToken?: MOAuthTokenUser;

  constructor(logger: LoggerInterface, webserverUrl: string) {
    this.logger = logger;
    this.webserverUrl = new URL(webserverUrl);
  }

  async getConfigAbout() {
    const response = await this.get<PeerTubeAboutConfigInterface>('/config/about');
    if (isAnErrorResponse(response)) {
      throw new Error(`Couldn't fetch instance configuration.`);
    }

    return response;
  }

  getInstanceLanguages() {
    return this.getConfigAbout().then(({ instance: { languages } }) => languages);
  }

  async getInstanceLanguage() {
    const instanceLanguages = await this.getInstanceLanguages();
    return instanceLanguages[0];
  }

  async hasVideoCaptionForLanguage(uuid: string, language: string) {
    const response = await this.get<ResultList<VideoCaption>>(`/videos/${uuid}/captions`);

    if (isAnErrorResponse(response)) {
      throw new Error(`Couldn't fetch video subtitles.`);
    }

    return response.data.findIndex(({ language: { id } }) => language === id) !== -1;
  }

  addVideoCaption(id: number, language: string, captionFile: any) {
    const formData = new FormData();
    formData.append('captionfile', captionFile);
    return request(
      {
        host: this.webserverUrl.hostname,
        port: this.webserverUrl.port,
        path: `${this.getPathPrefix()}/videos/${id}/captions/${language}`,
        protocol: this.webserverUrl.protocol,
        method: 'PUT',
        headers: this.getOAuthHeaders(),
      },
      formData
    );
  }

  setOAuthToken(oAuthToken: MOAuthTokenUser) {
    this.oAuthToken = oAuthToken;
  }

  setOAuthTokenFromResponse(response: AuthenticatedPeerTubeResponse) {
    this.setOAuthToken(PeerTubeApi.getOauthTokenFromResponse(response));
  }

  private static getOauthTokenFromResponse(response: AuthenticatedPeerTubeResponse): MOAuthTokenUser {
    return response.locals?.oauth?.token;
  }

  getOAuthHeaders() {
    if (this.oAuthToken) {
      return {
        Authorization: `Bearer ${this.oAuthToken.accessToken}`,
      };
    }
  }

  private async get<E extends any>(endpoint: string, version: number = 1): Promise<E | ErrorResponseInterface> {
    const url = `${this.webserverUrl.protocol}//${this.webserverUrl.host}${this.getPathPrefix(version)}${endpoint}`;
    this.logger.debug(`fetching ${endpoint} from ${url}`);
    const responseBody = await get(url, this.getOAuthHeaders());
    const parsedResponseBody = JSON.parse(responseBody as string);

    if (isAnErrorResponse(parsedResponseBody)) {
      this.logger.error(parsedResponseBody.error, parsedResponseBody);
    }

    return parsedResponseBody;
  }

  getPathPrefix(version: number = 1) {
    return `/api/v${version.toString()}`;
  }
}
