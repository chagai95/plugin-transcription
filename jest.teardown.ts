export default () =>
  new Promise((resolve, reject) => {
    if (global.server) {
      return global.server.close((e) => {
        console.log('Shutting down test server.');
        if (e) {
          reject(e);
        }
        resolve(null);
        // done();
      });
    }
    reject(new Error(`Couldn't find a running test server.`));
  });
